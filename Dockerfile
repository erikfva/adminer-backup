FROM mcr.microsoft.com/playwright:focal
USER root

WORKDIR /usr/src/app

COPY package*.json ./
# Install Playwright dependancies
RUN npm ci

COPY index.js .