# adminer-backup

Automation for the backup process using adminer.php as this application does not implement an API to do so.

## Dev

### Run in local

1. Rename file .env.example to .env and configure environment variables.

2. Execute

```
docker-compose up
```

3. Execute

```
docker-compose exec adminer-backup npm run start
```

### Modify

After modify the code rebuild the docker image executing

```
docker-compose build
```

## Ref

https://medium.com/devops-with-valentine/how-to-build-a-docker-image-and-push-it-to-the-gitlab-container-registry-from-a-gitlab-ci-pipeline-acac0d1f26df
