require("dotenv").config();
const { chromium } = require("playwright");
//requiring path and fs modules
const path = require("path");
//const fs = require("fs");
const { promises: fs } = require("fs");

const formatTwoDigits = (num) =>
  (parseInt(num) < 10 ? "0" : "") + num.toString();

const timeId = () => {
  const now = new Date();
  return (
    now.getFullYear().toString() +
    formatTwoDigits(now.getMonth() + 1) +
    formatTwoDigits(now.getDate()) +
    formatTwoDigits(now.getHours()) +
    formatTwoDigits(now.getMinutes()) +
    formatTwoDigits(now.getSeconds()) +
    now.getMilliseconds()
  );
};

const logger = (msg) => {
  console.log(msg, new Date().toString());
};

const {
  SERVER_ALWAYSDATA,
  ALWAYSDATA_USER,
  ALWAYSDATA_PASS,
  DATABASE_NAME,
} = process.env;
const DIR_BACKUP = process.env.DIR_BACKUP || path.join(__dirname, "files");
const BACKUP_FILES_COUNT = process.env.BACKUP_FILES_COUNT
  ? parseInt(process.env.BACKUP_FILES_COUNT)
  : 2;
const FULL_FILENAME = path.join(DIR_BACKUP, timeId() + "-backup.zip");

console.log({
  SERVER_ALWAYSDATA,
  ALWAYSDATA_USER,
  DATABASE_NAME,
});

(async () => {
  let browser = null;
  try {
    logger("Start the backup process...");

    browser = await chromium.launch({
      headless: true,
      args: ["--disable-dev-shm-usage"],
    });

    const context = await browser.newContext();
    const page = await context.newPage();
    const navigationPromise = page.waitForNavigation({
      waitUntil: "domcontentloaded",
    });
    await page.setDefaultNavigationTimeout(0);
    await page.goto(SERVER_ALWAYSDATA);
    await navigationPromise;

    await page.fill('input[name="login"]', ALWAYSDATA_USER);
    await page.fill('input[name="password"]', ALWAYSDATA_PASS);
    await page.click('button[type="submit"]');

    // Goto database
    await page.click(`a[href*="/structure&db=${DATABASE_NAME}"]`);

    await page.click('a[href*="/export&db="]');

    await page.click('input[value="custom"][type="radio"]');
    await page.selectOption('select[name="what"]', 'sql');
    await page.selectOption('select[name="compression"]', 'gzip');

    logger("downloading...");
    const [download] = await Promise.all([
        page.waitForEvent('download'), // wait for download event
        page.click('input[id="buttonGo"]') // start download
    ]);

    await fs.mkdir(DIR_BACKUP, { recursive: true });
    logger("saving... " + FULL_FILENAME);
    await download.saveAs(FULL_FILENAME);
    // wait for the download and delete the temporary file
    await download.delete();

    //passsing directoryPath and callback function
    const files = await fs.readdir(DIR_BACKUP);
    logger("keeping only " + BACKUP_FILES_COUNT + " files... ");
    files.sort().forEach(async function (file, index) {
      if (index < files.length - BACKUP_FILES_COUNT) {
        // Do whatever you want to do with the file
        const fullFileName = path.join(DIR_BACKUP, file);
        console.log("deleting...", fullFileName);
        await fs.unlink(fullFileName);
      }
    });
  } catch (error) {
    console.log("I'm sorry", error);
  } finally {
    console.log("Finish the backup process...", new Date().toString());
    if (browser !== null) {
      await browser.close();
    }
  }
})();
